<?php

namespace Roots\Sage\Extras;

use Roots\Sage\Setup;

/**
 * Add <body> classes
 */
function body_class($classes) {
  // Add page slug if it doesn't exist
  if (is_single() || is_page() && !is_front_page()) {
    if (!in_array(basename(get_permalink()), $classes)) {
      $classes[] = basename(get_permalink());
    }
  }

  // Add class if sidebar is active
  if (Setup\display_sidebar()) {
    $classes[] = 'sidebar-primary';
  }

  return $classes;
}
add_filter('body_class', __NAMESPACE__ . '\\body_class');

/**
 * Clean up the_excerpt()
 */
function excerpt_more() {
  return ' &hellip; <a href="' . get_permalink() . '">' . __('Continued', 'sage') . '</a>';
}
add_filter('excerpt_more', __NAMESPACE__ . '\\excerpt_more');

/**
 * Change default media folder
 */
update_option('uploads_use_yearmonth_folders', 0);
update_option('upload_path', 'media');

/**
 * Custom Slug
 */
function custom_slug($slug) {
  $slug = strtolower($slug);
  $slug = preg_replace("/[^a-z0-9_\s-]/", "", $slug);
  $slug = preg_replace("/[\s-]+/", " ", $slug);
  $slug = preg_replace("/[\s_]/", "-", $slug);
  return $slug;
}

// Causing Fluid Animation
function fluid_animation() {
  if(is_front_page()): ?>
  <script type="text/javascript">
    jQuery(document).ready(function () {
      jQuery("#myNavbar a").on('click', function (event) {
        if (this.hash !== "") {
          event.preventDefault();
          var hash = this.hash;
          if (jQuery(hash).length) {
            jQuery('html, body').animate({
              scrollTop: jQuery(hash).offset().top - jQuery(".top-header").outerHeight()
            }, 800, function () {
              if (jQuery(window).scrollTop() !== jQuery(hash).offset().top - jQuery(".top-header").outerHeight()) {
                jQuery('html, body').animate({
                  scrollTop: jQuery(hash).offset().top - jQuery(".top-header").outerHeight()
                }, 100);
              }
            });
          }

        }
      });
    });
  </script>

<?php endif;
}
add_action('wp_footer', __NAMESPACE__ . '\\fluid_animation', 20);