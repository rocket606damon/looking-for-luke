<!--<style>
.nyc .save-top h1 {
	font-size: 36px;
}
.clay-center-logo img {
	max-width: 52%;
}
.cf:after {
	content: '';
	display: block;
	clear: both;
}
.clay-link {
	width: 50%;
	float: left;
	text-align: right;
}
.clay-right {
  width: 40%;
  float: left;
  text-align: left;
  margin-left: 30px;
  margin-top: 30px;
}
.laureates {
	margin-top: 10px;
}
	.laureates img {
		width: 45%;
	}
.looking-for-luke-logo img {
	max-width: 60%;
}	
.nyc p {
	font-size: 1.2em;
}
.nyc .save-details {
	margin-top: 10px;
}
.nyc .save-details p:nth-of-type(2) {
  font-size: 20px;
  color: #ffc70e;
  font-weight: bold;
}
.nyc .save-details h3.name {
	font-size: 36px;
}
h4.hashtag {
	font-size: 2.15em;
}
.nyc .save-bottom .logos p {
	color: black;
}
.nyc .item {
	padding: 5px;
}
.logos p {
	margin-top: 30px;
}

/* Logos section */
.cafamh .item img {
  max-width: 40%;
}
.langone .item img, 
.csaah .item img {
  margin-top: 20px;
}
.ua3 .item img {
  max-width: 40%;
  margin-top: 30px;
}
.jlin .item img {
  max-width: 50%;
  margin-top: 10px;
}
.afsp .item img {
  margin-top: 20px;
}

.logos .col-sm-3.col-md-3 {
  width: 20%;
}
.cams .item img {
  margin-top: 18px;
}
.aabany .item img {
  max-width: 80%;
  margin-top: 22px;
}
.nyccap .item img {
  margin-top: 20px;
}
.dept .item img {
  max-width: 85%;
  margin-top: 20px;
}
.save-bottom .logos:last-child {
  margin-bottom: 30px;
}
@media screen and (max-width: 990px) {
	.clay-center-logo img {
	  max-width: 60%;
	}
}
@media screen and (max-width: 768px) {
	.nyc .save-top h1 {
		font-size: 36px;
	}
	.clay-center-logo {
		padding: 30px 0 10px;
	}
	.clay-link {
		width: 100%;
		float: none;
		text-align: center;
	}
	.clay-right {
    width: 100%;
    float: none;
    text-align: center;
    margin-left: 0px;
    margin-top: 20px;
	}
	.nyc .save-details h3.time {
		font-size: 38px;
	}
	.nyc .save-details p:nth-of-type(2){
		font-size: 16px;
	}
	.logos .col-sm-3.col-md-3 {
    width: 45%;
    margin: 0 auto;
	}
	.logos .bottom {
    width: 60%;
    margin: 0 auto;
	}
}
</style>-->

<section class="save-top">
	<h1>A Special Screening Event</h1>
</section>

<div class="overlay">
<div class="inner container">
<div class="content-middle" style="background: url('<?php bloginfo('url'); ?>/media/nyc/LFL-recolored.jpg') 50% no-repeat; background-size: cover;">
<section class="save-laureates">
	<div class="clay-center-logo cf">
		<a class="clay-link" href="https://www.mghclaycenter.org/" target="_blank">
			<img src="<?php bloginfo('url'); ?>/media/nyc/ClayCenter_4px_sroke.png" />
		</a>
		<p class="clay-right">Invites You to a Free Screening of</p>
	</div>
	<div class="laureates">
		<img src="<?php bloginfo('url'); ?>/media/nyc/laurels-fromdesign.png" />
	</div>
	<div class="looking-for-luke-logo">
		<img src="<?php bloginfo('url'); ?>/media/nyc/white_lfl_final_hi_res.png" />
	</div>
</section>
<section class="save-details">
	<p>Featuring Q&A with NBA Basketball Player</p>
	<h3 class="name">Jeremy Lin</h3>
	<h3 class="time">Sunday, September 16th</h3>
	<p>Doors open at 2:30pm. Screening begins at 3:00pm.</p>
	<p>Farkas Auditorium at NYU Langone Medical Center <br> 550 First Avenue, New York, NY 10016</p>
</section>
<section class="save-info">
	<p>"Looking for Luke" is a short documentary following the parents of Luke Tang, a well-liked, passionate and brilliant Harvard sophomore who took his family and friends by surprise when he died by suicide. Luke's parents, Wendell and Christina, have made it their mission to help other parents, particularly Asian parents, identify and understand the signs and signals of depression and other behavioral health disorders that can lead to suicide.</p>
	<h4 class="site"><a href="https://www.eventbrite.com/e/looking-for-luke-screening-in-new-york-city-tickets-48622616555?aff=efbeventtix" target="_blank">Register</a></h4>
	<p>Please join us for a special event promoting mental health<br>and wellness within the Asian American community.</p>
	<h4 class="hashtag">#LookingForLukeFilm</h4>
</section>
</div><!-- content middle -->
</div>
</div>

<div class="container">
<section class="save-bottom">
	<div class="logos first col-sm-12">
		<p>Hosting Organizations </p>
		<div class="row">
		<div class="col-sm-4 col-md-4 cafamh">
			<div class="item">
				<a href="http://www.cafamh.org/" target="_blank">
					<img src="<?php bloginfo('url'); ?>/media/nyc/cafamh-logo.jpg" />
				</a>
			</div>
		</div>
		<div class="col-sm-4 col-md-4 langone">
			<div class="item">
				<a href="https://nyulangone.org/" target="_blank">
					<img src="<?php bloginfo('url'); ?>/media/nyc/nyu-langone.png" />
				</a>
			</div>
		</div>
		<div class="col-sm-4 col-md-4 csaah">
			<div class="item">
				<a href="https://med.nyu.edu/asian-health/frontpage" target="_blank">
					<img src="<?php bloginfo('url'); ?>/media/nyc/nyu-csaah.jpg" />
				</a>
			</div>
		</div>
		</div>
		<div class="row">
		<div class="col-sm-4 col-md-4 ua3">
			<div class="item">
				<a href="https://www.ua3now.org/" target="_blank">
					<img src="<?php bloginfo('url'); ?>/media/nyc/UA3.png" />
				</a>
			</div>
		</div>
		<div class="col-sm-4 col-md-4 jlin">
			<div class="item">
				<a href="https://www.jlin7.com/" target="_blank">
					<img src="<?php bloginfo('url'); ?>/media/nyc/jlin-logo.png" />
				</a>
			</div>
		</div>
		<div class="col-sm-4 col-md-4 afsp">
			<div class="item">
				<a href="https://afsp.org/" target="_blank">
					<img src="<?php bloginfo('url'); ?>/media/nyc/AFSP.png" />
				</a>
			</div>
		</div>
		</div>
	</div>
	<div class="logos col-sm-12">
		<p>Community Sponsors</p>
		<div class="col-sm-3 col-md-3 caamh">
			<div class="item">
				<a href="http://www.asianmentalhealth.org/" target="_blank">
					<img src="<?php bloginfo('url'); ?>/media/nyc/NY-CAAMH.png" />
				</a>
			</div>
		</div>
		<div class="col-sm-3 col-md-3 cams">
			<div class="item">
				<a href="http://www.camsociety.org/" target="_blank">
					<img src="<?php bloginfo('url'); ?>/media/nyc/CAMS-CAIPA.jpg" />
				</a>
			</div>
		</div>
		<div class="col-sm-3 col-md-3 aabany">
			<div class="item">
				<a href="https://www.aabany.org/" target="_blank">
					<img src="<?php bloginfo('url'); ?>/media/nyc/AABANY-Logo.jpg" />
				</a>
			</div>
		</div>
		<div class="col-sm-3 col-md-3 nyccap">
			<div class="item">
				<a href="http://www.nyccap.org/home.aspx" target="_blank">
					<img src="<?php bloginfo('url'); ?>/media/nyc/nyccap.jpg" />
				</a>
			</div>
		</div>
		<div class="col-sm-3 col-md-3 dept">
			<div class="item">
				<a href="https://www1.nyc.gov/site/dca/index.page" target="_blank">
					<img src="<?php bloginfo('url'); ?>/media/nyc/nyc-dept-health.png" />
				</a>
			</div>
		</div>
	</div>
	<div class="logos col-sm-12">
		<p>Additional support for the film provided by: </p>
		<div class="col-sm-4 col-md-4 bottom samhsa">
			<div class="item">
				<a href="https://www.samhsa.gov/" target="_blank">
					<img src="<?php bloginfo('url'); ?>/media/nyc/SAMHSA_main_logo.png" />
				</a>
			</div>
		</div>
		<div class="col-sm-4 col-md-4 bottom hms">
			<div class="item">
				<a href="https://hms.harvard.edu/" target="_blank">
					<img src="<?php bloginfo('url'); ?>/media/nyc/harvard_hms_logo.png" />
				</a>
			</div>
		</div>
		<div class="col-sm-4 col-md-4 bottom mgh">
			<div class="item">
				<a href="https://www.massgeneral.org/" target="_blank">
					<img src="<?php bloginfo('url'); ?>/media/nyc/mgh_logo.jpg" />
				</a>
			</div>
		</div>
	</div>
</section>
</div>