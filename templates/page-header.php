<?php use Roots\Sage\Titles; ?>

<div class="page-header">
  <h1><?= Titles\title(); ?></h1>
  <?php if(is_page('host-a-screening')): ?>
  <p class="screening-subtitle">The mission of this film is to raise awareness of depression as an illness, and to destigmatize the seeking of help for mental health issues. To request a screening of the film at your school or organization, please fill out the contact form and we’ll be in touch with you shortly.</p>
  <?php endif; ?>	
</div>
