<head>
  <?php if(is_page('host-a-screening')): ?>
  <title>"Looking for Luke" Film: Host a Screening</title>
  <?php elseif(is_page('nyc')): ?>
  <title>New York City | "Looking for Luke" Film</title>
  <?php else: ?>
  <title>"Looking for Luke" Film</title>
  <?php endif; ?> 
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta property="og:url" content="http://lookingforlukefilm.com" />
  <meta property="og:title" content="&quot;Looking for Luke&quot; Film" />
  <meta property="og:description" content="A documentary following a family's journey as they uncover the truth about their son's suicide." />
  <meta property="og:image" content="/media/Luke_Thinking.jpg" />
  <?php wp_head(); ?>
  <script src="https://use.typekit.net/mge3aqm.js"></script>
  <script>try{Typekit.load({ async: true });}catch(e){}</script>
</head>
