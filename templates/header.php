<?php if(is_front_page()): ?>
<header id="home">
  <div class="top-header navbar-fixed-top" data-spy="affix" data-offset-top="10">
    <div class="container">
      <div class="row">
        <div class="col-md-4 col-sm-3 col-xs-6 left-logo"><a href="<?php echo get_home_url(); ?>" title="Looking for Luke" class="logo"><img src="/media/logo.png" alt="Looking for Luke"></a></div>
        <!-- menu Start -->
        <div class="col-md-8 col-sm-9 col-xs-6 rght-menu">
          <div class="social-media">
            <ul class="clearfix">
              <li><a href="https://www.facebook.com/lookingforlukefilm" class="fa fa-facebook" title="Facebook" target="_blank"></a></li>
              <li><a href="https://twitter.com/LFL_Film" class="fa fa-twitter" title="Twitter" target="_blank"></a></li>
            </ul>
          </div>
          <div class="menu">
            <nav id="nav" class="navbar navbar-default">
              <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#myNavbar"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
              </div>
              <div class="collapse navbar-collapse" id="myNavbar">
                <ul class="nav navbar-nav navbar-center">
                  <li class="active"><a href="#home" title="the film">the film</a></li>
                  <li><a href="#screenings" title="screenings">screenings</a></li>
                  <li><a href="#involved" title="get involved">get involved</a></li>
                  <li><a href="#about" title="about">about</a></li>
                  <li><a href="#issue" title="the issue">the issue</a></li>
                  <li><a href="#resources" title="resources">resources</a></li>
                </ul>
              </div>
            </nav>
          </div>
        </div>
        <!-- menu Ends --> 
      </div>
    </div>
  </div>
  <div class="video-div">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <div class="video">
            <iframe width="560" height="315" src="https://www.youtube.com/embed/qipDNvyHn-8?playlist=cl1q7zRpX2Y&showinfo=0&controls=1&rel=0" allowfullscreen></iframe>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="the-film">
    <div class="container">
      <div class="row">
        <div class="col-md-6 col-sm-12 right-txt">
          <h1><?php the_field('the_film_title'); ?></h1>
          <?php the_field('the_film_content'); ?>
        </div>
        <div class="col-md-6 col-sm-12 lft-img"><img src="/media/face-img.png" alt="The Film"></div>
      </div>
    </div>
  </div>
</header>
<!-- header Ends -->
<?php else: ?>
<header id="home">
  <div class="top-header navbar-fixed-top" data-spy="affix" data-offset-top="10">
    <div class="container">
      <div class="row">
        <div class="col-md-4 col-sm-3 col-xs-6 left-logo"><a href="<?php echo get_home_url(); ?>" title="Looking for Luke" class="logo"><img src="/media/logo.png" alt="Looking for Luke"></a></div>
        <!-- menu Start -->
        <div class="col-md-8 col-sm-9 col-xs-6 rght-menu">
          <div class="social-media">
            <ul class="clearfix">
              <li><a href="https://www.facebook.com/lookingforlukefilm" class="fa fa-facebook" title="Facebook" target="_blank"></a></li>
              <li><a href="https://twitter.com/LFL_Film" class="fa fa-twitter" title="Twitter" target="_blank"></a></li>
            </ul>
          </div>
          <div class="menu">
            <nav id="nav" class="navbar navbar-default">
              <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#myNavbar"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
              </div>
              <div class="collapse navbar-collapse" id="myNavbar">
                <ul class="nav navbar-nav navbar-center">
                  <li class="active"><a href="<?php if(!is_front_page()) echo bloginfo('url') ?>#home" title="the film">the film</a></li>
                  <li><a href="<?php if(!is_front_page()) echo bloginfo('url') ?>#screenings" title="screenings">screenings</a></li>
                  <li><a href="<?php if(!is_front_page()) echo bloginfo('url') ?>#involved" title="get involved">get involved</a></li>
                  <li><a href="<?php if(!is_front_page()) echo bloginfo('url') ?>#about" title="about">about</a></li>
                  <li><a href="<?php if(!is_front_page()) echo bloginfo('url') ?>#issue" title="the issue">the issue</a></li>
                  <li><a href="<?php if(!is_front_page()) echo bloginfo('url') ?>#resources" title="resources">resources</a></li>
                </ul>
              </div>
            </nav>
          </div>
        </div>
        <!-- menu Ends --> 
      </div>
    </div>
  </div>
</header>
<?php endif; ?>   
