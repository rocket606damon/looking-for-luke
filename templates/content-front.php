<!-- mid-contant start -->
<style>
/*.disabled{
  pointer-events: none;
  cursor: default;
  opacity: .5;
}*/
</style>
<section class="mid-contant">
  <div class="upcomin-div" id="screenings">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <?php $i = 0; while (have_rows('screenings')) : the_row(); ?>
            <h3><?php the_sub_field('section_title'); ?></h3>
            <div class="<?php echo ($i == 0) ? 'screening-top' : 'screening-lower'; ?>">
          <?php $g = 0; while(have_rows('screening_info')) : the_row(); ?>
            <?php if($i !== 0): ?><div class="past-list"><?php endif; ?>
            <span><?php the_sub_field('date_of_screening'); ?></span>
            <p><?php the_sub_field('name_of_event'); ?></p>
            <?php if($i == 0): ?><div class="btn-list"> <a href="<?php the_sub_field('event_url'); ?>" title="view event details" class="red-btn" target="_blank">view event details</a> </div><?php endif; ?>
            <?php if($i !== 0): ?></div><?php endif; ?>
          <?php $g++; endwhile; ?> 
            </div>
          <?php $i++; endwhile; ?>
        </div>
      </div>
    </div>
  </div>
  <div class="get-div" id="involved">
    <div class="container">
      <div class="row">
        <h3><?php the_field('get_involved_title'); ?></h3>
        <div class="col-sm-6">
          <div class="involved-list">
            <h4><?php the_field('host_a_screening_title'); ?></h4>
            <div class="icon"> <img src="/media/host.png" alt="host"> </div>
            <?php the_field('host_a_screening_text'); ?>
          </div>
        </div>
        <div class="col-sm-6 hide">
          <div class="involved-list">
            <h4><?php the_field('share_with_others_title'); ?></h4>
            <div class="icon"> <img src="/media/share.png" alt="share"> </div>
            <?php the_field('share_with_others_text'); ?>
          </div>
        </div>
      <!-- </div>
      <div class="involved_nudge_space"></div>
      <div class="row">  --> 
        <div class="col-sm-6">
          <div class="involved-list">
            <h4><?php the_field('contact_us_title'); ?></h4>
            <div class="icon"> <img src="/media/contact-us.png" alt="contact"> </div>
            <?php the_field('contact_us_text'); ?> 
          </div>
        </div>
        <div class="col-sm-6 hide">
          <div class="involved-list">
            <h4><?php the_field('donate_title'); ?></h4>
            <div class="icon"> <img src="/media/donate.png" alt="donate"> </div>
            <?php the_field('donate_text'); ?>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="about-div" id="about">
    <div class="container">
      <div class="row">
        <h3><?php the_field('about_filmmakers_title'); ?></h3>
        <div class="clearfix">
          <div class="col-md-6 col-sm-6 filmaker-list">
            <div class="col-md-4 col-sm-12 col-xs-4 left-img"> <img src="/media/eric.jpg" alt="Eric I. Lu"> </div>
            <div class="col-md-8 col-xs-8 col-sm-12 right-text">
              <?php the_field('eric_bio'); ?>
            </div>
          </div>
          <div class="col-md-6 col-sm-6 filmaker-list">
            <div class="col-md-4 col-xs-4 col-sm-12 left-img"> <img src="/media/elaina.jpg" alt="Elaine Coin"> </div>
            <div class="col-md-8 col-xs-8 col-sm-12 right-text">
              <?php the_field('elaine_bio'); ?>
            </div>
          </div>
        </div>
        <div class="clearfix">
          <div class="col-md-6 col-sm-6 filmaker-list">
            <div class="col-md-4 col-xs-4 col-sm-12 left-img"> <img src="/media/juliana.jpg" alt="Juliana Chen, M.D."> </div>
            <div class="col-md-8 col-xs-8 col-sm-12 right-text">
              <?php the_field('juliana_bio'); ?>
            </div>
          </div>
          <div class="col-md-6 col-sm-6 filmaker-list">
            <div class="col-md-4 col-xs-4 col-sm-12 left-img"> <img src="/media/gene-beresin.jpg" alt="Gene Beresin, M.D., M.A."> </div>
            <div class="col-md-8 col-xs-8 col-sm-12 right-text">
              <?php the_field('gene_bio'); ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="clay-center-div">
    <div class="container">
      <div class="row">
        <div class="col-md-6 col-sm-6 col-xs-6 clay-img"> <img src="/media/claycenter-logo-2.png" alt="About the Clay Center for Young Healthy Minds"> </div>
        <div class="col-md-6 col-sm-8 col-xs-8 clay-text">
          <h4><?php the_field('clay_center_title'); ?></h4>
          <p><?php the_field('clay_center_description'); ?></p>
          <ul class="contact-link">
            <li class="site" style="background-image: url(/media/glob-icon.png);"> <a href="http://www.mghclaycenter.org/" target="_blank" title="Visit Site">www.mghclaycenter.org</a> </li>
            <li class="twit" style="background-image: url(/media/twit-icon.png);"> <a href="https://twitter.com/mghclaycenter?lang=en" target="_blank" title="Twitter">@MGHClayCenter</a> </li>
            <li class="facebk" style="background-image: url(/media/fb-icon.png);"> <a href="https://www.facebook.com/massgeneralclaycenter/" target="_blank" title="Facebook">facebook.com/massgeneralclaycenter</a> </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <div class="issue-div" id="issue">
    <div class="container">
      <div class="row">
        <div class="col-md-6 col-sm-6 issue-rt"><img src="/media/parents.png" alt="the parents"></div>
        <div class="col-md-6 col-sm-6 issue-lt">
          <h3><?php the_field('the_issue_title'); ?></h3>
          <?php the_field('the_issue_content'); ?>
        </div>
      </div>
    </div>
  </div>
  <div class="resources-div" id="resources">
    <div class="container">
      <div class="row">
        <h3>Resources</h3>
        <ul class="resource-list">
          <li class="clearfix">
            <div class="col-md-3 col-sm-4 col-xs-4 resource-img">
              <div class="rsrc-img-inner" style="background-image: url(/media/resource-5.jpg);"> </div>
            </div>
            <div class="col-md-7 col-sm-7 col-xs-7 resource-text"> <span>March 6, 2017</span>
              <h4><a href="http://www.mghclaycenter.org/parenting-concerns/college-mental-health-crisis-call-cultural-change-part-2/" title="The College Mental Health Crisis: A Call for Cultural Change - Part 2">The College Mental Health Crisis: A Call for Cultural Change - Part 2</a></h4>
            </div>
            <div class="col-md-2 col-sm-2 col-xs-2 next-link"> <a href="http://www.mghclaycenter.org/parenting-concerns/college-mental-health-crisis-call-cultural-change-part-2/" title="Read More"></a> </div>
          </li>
          <li class="clearfix">
            <div class="col-md-3 col-sm-4 col-xs-4 resource-img">
              <div class="rsrc-img-inner" style="background-image: url(/media/resource-4.jpg);"> </div>
            </div>
            <div class="col-md-7 col-sm-7 col-xs-7 resource-text"> <span>February 20, 2017</span>
              <h4><a href="http://www.mghclaycenter.org/parenting-concerns/6752/" title="The College Mental Health Crisis: A Call for Cultural Change - Part 1">The College Mental Health Crisis: A Call for Cultural Change - Part 1</a></h4>
            </div>
            <div class="col-md-2 col-sm-2 col-xs-2 next-link"> <a href="http://www.mghclaycenter.org/parenting-concerns/6752/" title="Read More"></a> </div>
          </li>
          <li class="clearfix">
            <div class="col-md-3 col-sm-4 col-xs-4 resource-img">
              <div class="rsrc-img-inner" style="background-image: url(/media/resource-1.jpg);"> </div>
            </div>
            <div class="col-md-7 col-sm-7 col-xs-7 resource-text"> <span>September 27, 2016</span>
              <h4><a href="http://www.mghclaycenter.org/hot-topics/talking-children-suicide/" title="Talking With Children About Suicide">Talking With Children About Suicide</a></h4>
            </div>
            <div class="col-md-2 col-sm-2 col-xs-2 next-link"> <a href="http://www.mghclaycenter.org/hot-topics/talking-children-suicide/" title="Read More"></a> </div>
          </li>
          <li class="clearfix">
            <div class="col-md-3 col-sm-4 col-xs-4 resource-img">
              <div class="rsrc-img-inner" style="background-image: url(/media/resource-2.jpg);"> </div>
            </div>
            <div class="col-md-7 col-sm-7 col-xs-7 resource-text"> <span>May 10, 2016</span>
              <h4><a href="http://www.mghclaycenter.org/hot-topics/decreased-stigma-mental-illness-among-american-public-not-among-clinicians/" title="Decreased Stigma of Mental Illness Among The American Public – But Not Among Clinicians">Decreased Stigma of Mental Illness Among The American Public – But Not Among Clinicians</a></h4>
            </div>
            <div class="col-md-2 col-sm-2 col-xs-2 next-link"> <a href="http://www.mghclaycenter.org/hot-topics/decreased-stigma-mental-illness-among-american-public-not-among-clinicians/" title="Read More"></a> </div>
          </li>
          <li class="clearfix">
            <div class="col-md-3 col-sm-4 col-xs-4 resource-img">
              <div class="rsrc-img-inner" style="background-image: url(/media/resource-3.jpg);"> </div>
            </div>
            <div class="col-md-7 col-sm-7 col-xs-7 resource-text"> <span>April 21, 2016</span>
              <h4><a href="http://www.mghclaycenter.org/parenting-concerns/teenagers/spring-suicide-an-unlikely-combination/" title="Spring Suicide: An (Un)Likely Combination?">Spring Suicide: An (Un)Likely Combination?</a></h4>
            </div>
            <div class="col-md-2 col-sm-2 col-xs-2 next-link"> <a href="http://www.mghclaycenter.org/parenting-concerns/teenagers/spring-suicide-an-unlikely-combination/" title="Read More"></a> </div>
          </li>
        </ul>
        <div class="visite-btn"> <a href="http://www.mghclaycenter.org/tag/LFL/" title="visit clay center for more resources">visit the clay center for more resources</a> </div>
      </div>
    </div>
  </div>
  <div class="gallery-div" id="photo-gallery">
    <div class="container">
      <div class="row">
        <h3>Photo Gallery</h3>

        <!--image 1-->
        <div class="col-sm-4 col-xs-6 col-md-3 col-lg-3">
          <a class="thumbnail fancybox" data-fancybox="boxalight" href="/media/gallery/Parents_Sunset.jpg">
            <img class="img-responsive" alt="parents sunset" src="/media/gallery/Parents_Sunset.jpg" />
          </a>
        </div>

        <!--image 2-->
        <div class="col-sm-4 col-xs-6 col-md-3 col-lg-3">
          <a class="thumbnail fancybox" data-fancybox="boxalight" href="/media/gallery/Parents_at_Dinner_Table.jpg">
            <img class="img-responsive" alt="family-at-grave" src="/media/gallery/Parents_at_Dinner_Table.jpg" />
          </a>
        </div>

        <!--image 3-->
        <div class="col-sm-4 col-xs-6 col-md-3 col-lg-3">
          <a class="thumbnail fancybox" data-fancybox="boxalight" href="/media/gallery/Luke_Black_and_White.jpg">
            <img class="img-responsive" alt="family-at-grave" src="/media/gallery/Luke_Black_and_White.jpg" />
          </a>
        </div>

        <!--image 4-->
        <div class="col-sm-4 col-xs-6 col-md-3 col-lg-3">
          <a class="thumbnail fancybox" data-fancybox="boxalight" href="/media/gallery/Mom_Solo_Interview.jpg">
            <img class="img-responsive" alt="family-at-grave" src="/media/gallery/Mom_Solo_Interview.jpg" />
          </a>
        </div>

        <!--image 5-->
        <div class="col-sm-4 col-xs-6 col-md-3 col-lg-3">
          <a class="thumbnail fancybox" data-fancybox="boxalight" href="/media/gallery/Gravestone_Love.jpg">
            <img class="img-responsive" alt="family-at-grave" src="/media/gallery/Gravestone_Love.jpg" />
          </a>
        </div>

        <!--image 6-->
        <div class="col-sm-4 col-xs-6 col-md-3 col-lg-3">
          <a class="thumbnail fancybox" data-fancybox="boxalight" href="/media/gallery/Luke_Graduation.jpg">
            <img class="img-responsive" alt="family-at-grave" src="/media/gallery/Luke_Graduation.jpg" />
          </a>
        </div>

        <!--image 7-->
        <div class="col-sm-4 col-xs-6 col-md-3 col-lg-3">
          <a class="thumbnail fancybox" data-fancybox="boxalight" href="/media/gallery/Linda_Interview.jpg">
            <img class="img-responsive" alt="family-at-grave" src="/media/gallery/Linda_Interview.jpg" />
          </a>
        </div>

        <!--image 8-->
        <div class="col-sm-4 col-xs-6 col-md-3 col-lg-3">
          <a class="thumbnail fancybox" data-fancybox="boxalight" href="/media/gallery/Luke_Painting_in_Kitchen.jpg">
            <img class="img-responsive" alt="family-at-grave" src="/media/gallery/Luke_Painting_in_Kitchen.jpg" />
          </a>
        </div>

        <!--image 9-->
        <div class="col-sm-4 col-xs-6 col-md-3 col-lg-3">
          <a class="thumbnail fancybox" data-fancybox="boxalight" href="/media/gallery/Dad_Reading_Journal.jpg">
            <img class="img-responsive" alt="family-at-grave" src="/media/gallery/Dad_Reading_Journal.jpg" />
          </a>
        </div>

        <!--image 10-->
        <div class="col-sm-4 col-xs-6 col-md-3 col-lg-3">
          <a class="thumbnail fancybox" data-fancybox="boxalight" href="/media/gallery/Flowers.jpg">
            <img class="img-responsive" alt="family-at-grave" src="/media/gallery/Flowers.jpg" />
          </a>
        </div>

        <!--image 11-->
        <div class="col-sm-4 col-xs-6 col-md-3 col-lg-3">
          <a class="thumbnail fancybox" data-fancybox="boxalight" href="/media/gallery/Richard_Interview.jpg">
            <img class="img-responsive" alt="family-at-grave" src="/media/gallery/Richard_Interview.jpg" />
          </a>
        </div>

        <!--image 12-->
        <div class="col-sm-4 col-xs-6 col-md-3 col-lg-3">
          <a class="thumbnail fancybox" data-fancybox="boxalight" href="/media/gallery/Family_at_Grave.jpg">
            <img class="img-responsive" alt="family-at-grave" src="/media/gallery/Family_at_Grave.jpg" />
          </a>
        </div>

      </div>
    </div>
  </div>
</section>
<!-- mid-contant Ends -->

<!-- Modal Section Starts -->

<div id="hostscreening" class="modal fade join-email-list" role="dialog" tab-index="-1" aria-hidden="true">
  <div class="modal-dialog"> 
    
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">host a screening</h4>
      </div>
      <div class="modal-body">
        <form action="#">
          <div class="felment-wrapper">
            <input type="text" class="textbox felment" placeholder="Name" id="name" tabindex="1" name="name">
          </div>
          <div class="felment-wrapper">
            <input type="text" class="textbox felment" placeholder="Email" id="email" tabindex="2" name="email">
          </div>
          <div class="felment-wrapper">
            <input class="submit-button" type="submit" title="Log In" value="Submit" tabindex="4">
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade join-email-list" id="joinemail" role="dialog" tabindex="-1" aria-hidden="true">
  <div class="modal-dialog"> 
    
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title">join email list</h4>
      </div>
      <div class="modal-body">
        <!--Begin CTCT Sign-Up Form-->
        <!-- EFD 1.0.0 [Tue Nov 29 16:23:45 EST 2016] -->
        <link rel='stylesheet' type='text/css' href='https://static.ctctcdn.com/h/contacts-embedded-signup-assets/1.0.2/css/signup-form.css'>
        <div class="ctct-embed-signup" style="font: 16px museo-sans, Helvetica Neue, Arial, sans-serif; font: 1rem Helvetica Neue, Arial, sans-serif; line-height: 1.5; -webkit-font-smoothing: antialiased;">
         <div style="color:#4C4C4C; background-color:#FFFFFF; border-radius:5px;">
           <span id="success_message" style="display:none;">
               <div style="text-align:center;">Join our email list for updates about the film, screenings, and mental health resources.</div>
           </span>
           <form data-id="embedded_signup:form" class="ctct-custom-form Form" name="embedded_signup" method="POST" action="https://visitor2.constantcontact.com/api/signup">
               <h2 style="margin-bottom:10px;">Learn more about the new documentary!</h2>
               <!-- The following code must be included to ensure your sign-up form works properly. -->
               <input data-id="ca:input" type="hidden" name="ca" value="d884c9f2-1a66-47b6-8a86-7f9ebcb9e67d">
               <input data-id="list:input" type="hidden" name="list" value="2044764451">
               <input data-id="source:input" type="hidden" name="source" value="EFD">
               <input data-id="required:input" type="hidden" name="required" value="list,email,first_name,address_city,address_state">
               <input data-id="url:input" type="hidden" name="url" value="">
               <p data-id="Email Address:p" ><label data-id="Email Address:label" data-name="email" class="ctct-form-required">Email Address</label> <input data-id="Email Address:input" type="text" name="email" value="" maxlength="80"></p>
               <p data-id="First Name:p" ><label data-id="First Name:label" data-name="first_name" class="ctct-form-required">First Name</label> <input data-id="First Name:input" type="text" name="first_name" value="" maxlength="50"></p>
               <p data-id="City:p" ><label data-id="City:label" data-name="address_city" class="ctct-form-required">City</label> <input data-id="City:input" type="text" name="address_city" value="" maxlength="50"></p>
               <p data-id="State/Province:p" ><label data-id="State/Province:label" data-name="address_state" class="ctct-form-required">Select a state/province</label></p>
               <button type="submit" class="Button ctct-button Button--block Button-secondary" data-enabled="enabled">Sign Up</button>
               <div>
                 <p class="ctct-form-footer">By submitting this form, you are granting: The MGH Clay Center for Young Healthy Minds, One Bowdoin Square, Boston, Massachusetts, 02114, United States, http://www.mghclaycenter.org permission to email you. You may unsubscribe via the link found at the bottom of every email.  (See our <a href="http://www.constantcontact.com/legal/privacy-statement" target="_blank">Email Privacy Policy</a> for details.) Emails are serviced by Constant Contact.</p>
               </div>
           </form>
         </div>
        </div>
      <script type='text/javascript'>
         var localizedErrMap = {};
         localizedErrMap['required'] =    'This field is required.';
         localizedErrMap['ca'] =      'An unexpected error occurred while attempting to send email.';
         localizedErrMap['email'] =       'Please enter your email address in name@email.com format.';
         localizedErrMap['birthday'] =    'Please enter birthday in MM/DD format.';
         localizedErrMap['anniversary'] =   'Please enter anniversary in MM/DD/YYYY format.';
         localizedErrMap['custom_date'] =   'Please enter this date in MM/DD/YYYY format.';
         localizedErrMap['list'] =      'Please select at least one email list.';
         localizedErrMap['generic'] =     'This field is invalid.';
         localizedErrMap['shared'] =    'Sorry, we could not complete your sign-up. Please contact us to resolve this.';
         localizedErrMap['state_mismatch'] = 'Mismatched State/Province and Country.';
        localizedErrMap['state_province'] = 'Select a state/province';
         localizedErrMap['selectcountry'] =   'Select a country';
         var postURL = 'https://visitor2.constantcontact.com/api/signup';
      </script>
      <script type='text/javascript' src='https://static.ctctcdn.com/h/contacts-embedded-signup-assets/1.0.2/js/signup-form.js'></script>
      <!--End CTCT Sign-Up Form-->
      </div>
    </div>
  </div>
</div>
<!-- Modal Section Ends -->