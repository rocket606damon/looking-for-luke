<footer>
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-md-offset-2">
        <div class="footer-logo"><a href="<?php echo get_home_url(); ?>" title="Looking for Luke"><img src="/media/logo.png" alt="Looking for Luke"></a></div>
        <ul class="footer-link">
          <li><a href="<?php if(!is_front_page()) echo bloginfo('url') ?>#home" title="the film">the film</a></li>
          <li><a href="<?php if(!is_front_page()) echo bloginfo('url') ?>#screenings" title="screenings">screenings</a></li>
          <li><a href="<?php if(!is_front_page()) echo bloginfo('url') ?>#involved" title="get involved">get involved</a></li>
          <li><a href="<?php if(!is_front_page()) echo bloginfo('url') ?>#about" title="about">about</a></li>
          <li><a href="<?php if(!is_front_page()) echo bloginfo('url') ?>#issue" title="the issue">the issue</a></li>
          <li><a href="<?php if(!is_front_page()) echo bloginfo('url') ?>#resources" title="resources">resources</a></li>
        </ul>
        <div class="copyright">©2017. all rights reserved.</div>
      </div>
    </div>
  </div>
</footer>