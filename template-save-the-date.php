<?php
/**
 * Template Name: Save The Date Template
 */
?>

<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/page', 'header'); ?>
  <?php get_template_part('templates/content', 'save-the-date'); ?>
<?php endwhile; ?>